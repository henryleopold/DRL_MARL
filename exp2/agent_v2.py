#===============================================================================
# Imports
#-------------------------------------------------------------------------------
import random, json, numpy as np

from model import Actor, Critic, OUNoise
from memory import ReplayBuffer
import utils

import torch
import torch.nn.functional as F
import torch.optim as optim

#===============================================================================
# Variables
#-------------------------------------------------------------------------------

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

#===============================================================================
# Agent Class
#-------------------------------------------------------------------------------
class Learner:
    """ Base Learner class.
    Args:
        memory_size (int, default=1e5): Replay buffer size.
        batch_size (int, default=64): minibatch size.
        gamma (float, default=0.995): Discount factor hyperparameter.
        tau (float, default=1e-3): Hyperparameter for soft update of target parameters.
        shared_memory (boolean, default=True): If agents share the learner's memory or if they have their own.
        filepath (string, default='logdir/agent-checkpoint'): Save/load file path without extension.
        random_seed (int, default=0): Random seed.
    """
     
    def __init__(self,   action_size, 
                         memory_size = 1e6,
                         batch_size = 128,
                         shared_memory = True,
                         filepath = 'logdir/learner-checkpoint',
                         random_seed=0
                ):
        
        # Basic Params
        self.action_size = action_size
        self.num_agents = 0
        self.agents = []
        self.t = 0
        self.seed = random.seed(random_seed)
        self.filepath = filepath
        
        # Memory Params
        self.batch_size = int(batch_size)
        self.memory_size = int(memory_size)
        self.memory = ReplayBuffer(action_size, int(memory_size), int(batch_size), random_seed) if shared_memory else None
        self.shared_memory = shared_memory
    
    #---------------------------------------------------------------------------
    # Class functions
    #---------------------------------------------------------------------------
    def add(self, agent):
        """ Add agents to the learner """
        self.agents.append(agent)
        self.num_agents += 1
    
        
    #---------------------------------------------------------------------------
    # Learning functions
    #---------------------------------------------------------------------------
    def act(self, states, add_noise=True):
        """Returns actions for given state for each agents' current policy."""
        actions = np.zeros((self.num_agents, self.action_size))
        for e, agent in enumerate(self.agents):
            actions[e,:] += agent.act(states[e, :], add_noise)        
        return actions
    
    def step(self, states, actions, rewards, next_states, dones):
        """Save experience in replay memory, and use random sample from buffer to learn."""
        for e, agent in enumerate(self.agents):            
            self.memory.add(states[e,:], actions[e, :], rewards[e], next_states[e,:], dones[e])
        
            if len(self.memory) > self.batch_size:
                if self.shared_memory:
                    experience = self.memory.sample()
                else:
                    experience = agent.memory.sample()

                agent.learn(experience)

    def reset(self):
        """Reset all agents"""
        for agent in self.agents:
            agent.reset()

    #---------------------------------------------------------------------------
    # Save, Load, Checkpoint & State Related
    #---------------------------------------------------------------------------
    def save(self, filepath = None, sconf=False):
        """ Saves the Learner """
        if filepath is None: filepath = self.filepath
        for e, agent in enumerate(self.agents):
            torch.save(agent.actor_online.state_dict(), filepath+f'_agent{e+1}_actor.pickle')
            torch.save(agent.critic_online.state_dict(), filepath+f'_agent{e+1}_critic.pickle')
        print(f'\nLearner saved to {filepath}.')

    def load(self, filepath=None):
        """ Loads the Learner """
        if filepath is None: filepath = self.filepath
        for e, agent in enumerate(self.agents):
            agent.actor_online.state_dict(torch.load(filepath+f'checkpoint_agent{e+1}_actor.pickle'))
            agent.critic_online.state_dict(torch.load(filepath+f'checkpoint_agent{e+1}_critic.pickle'))
        print(f'\nLearner loaded from {filepath}.')

# ================================================================================        
# Agent class
# --------------------------------------------------------------------------------         
class Agent:
    """ Base agent class.
    Args:
        state_size (int): Dimension of each state.
        action_size (int): Dimension of each action.
        num_agents (int, default=1): Number of actors/bodies the agent manages 
        shared_memory (ReplayMemory, default=None): Shared Replay buffer object.
        memory (int, default=1e5): Replay buffer size if no shared buffer is provided.
        batch_size (int, default=64): minibatch size.
        gamma (float, default=0.995): Discount factor hyperparameter.
        tau (float, default=1e-3): Hyperparameter for soft update of target parameters.
        learning_rate_actor (float or torch.lr_schedule, default=5e-4): Initital learning rate or  / equivalent.
        learning_rate_critic (float, default=5e-4): Initital learning rate or  / equivalent.
        network_actor (Network Module, default=Actor): Policy Network class
        network_layers_actor (list of ints, default=[300,400]): Sizes for the layers matching the actor network.
        network_critic (Network Module, default=Critic): Policy Network class
        network_layers_critic (list of ints, default=[300,400]): Sizes for the layers matching the critic network.
        optimizer_actor (Optimizer, default=Adam): Compatible optimizer function
        optimizer_critic (Optimizer, default=Adam): Compatible optimizer function
        clip_critic (boolean, default=False): Flag for clipping critic gradient during learning.
        weight_decay (float, default=0.0001): L2 weight decay
        use_noise (boolean, default=True):
        filepath (string, default='logdir/agent-checkpoint'): Save/load file path without extension.
        random_seed (int, default=0): Random seed.
    """
     
    def __init__(self, state_size, 
                         action_size, 
                         shared_memory = None,
                         memory_size = int(1e6),
                         batch_size = 128,
                         gamma = 0.995,
                         tau = 1e-3,
                         learning_rate_actor = 5e-4,
                         learning_rate_critic = 3e-5,
                         network_actor = Actor,
                         network_layers_actor = [300,400],
                         network_critic = Critic,
                         network_layers_critic = [300,400],
                         optimizer_actor = optim.Adam,
                         optimizer_critic = optim.Adam,
                         clip_critic = False,
                         weight_decay = 0.00001,
                         filepath = 'logdir/agent-checkpoint',
                         use_noise = True,
                         random_seed=0
                ):
        
        # Basic Params
        self.state_size = state_size
        self.action_size = action_size
        self.seed = random.seed(random_seed)
        self.filepath = filepath
        
        # Memory Params
        self.batch_size = int(batch_size)
        self.memory_size = int(memory_size)
        self.memory = shared_memory if type(shared_memory) else ReplayBuffer(action_size, int(memory_size), int(batch_size), random_seed)
        
        # Learning Params
        self.gamma = gamma
        self.tau = tau
        self.weight_decay = weight_decay
        
        # Actor Network (w/ Target Network)
        self.actor_online = Actor(state_size, action_size, random_seed, *network_layers_actor).to(device)
        self.actor_internal = Actor(state_size, action_size, random_seed, *network_layers_actor).to(device)
        self.actor_learning_rate = learning_rate_actor
        self.actor_optimizer = optim.Adam(self.actor_online.parameters(), lr=learning_rate_actor)
        
        # Critic Network (w/ Target Network)
        self.critic_online = Critic(state_size, action_size, random_seed, *network_layers_critic).to(device)
        self.critic_internal = Critic(state_size, action_size, random_seed, *network_layers_critic).to(device)
        self.critic_learning_rate = learning_rate_critic
        self.critic_optimizer = optim.Adam(self.critic_online.parameters(), lr=learning_rate_critic, weight_decay=weight_decay)
        self.clip_critic = clip_critic
        
        # Noise process
        self.noise = OUNoise(action_size, random_seed)
        self.use_noise = use_noise
        
    #---------------------------------------------------------------------------
    # Learning functions
    #---------------------------------------------------------------------------
    def act(self, state, add_noise=True):
        """Returns actions for given state as per current policy."""
        state = torch.from_numpy(state).float().to(device)
        
        self.actor_online.eval()
        
        # All agents take random steps at first (find citation)
        with torch.no_grad():
            action = self.actor_online(state).cpu().data.numpy()
        self.actor_online.train()
        if add_noise:
            action += self.noise.sample()
        return np.clip(action, -1, 1)
    
    def step(self, state, action, reward, next_state, done):
        """Save experience in replay memory, and use random sample from buffer to learn."""
        # Save experience / reward
        self.memory.add(state, action, reward, next_state, done)

        # Learn, if enough samples are available in memory and at the right frequency
        if len(self.memory) > self.batch_size:
            experience = self.memory.sample()
            self.learn(experience)

    def learn(self, experience):
        """ Update policy and value parameters using given batch of experience tuples.
        Q_targets = r + γ * critic_internal(next_state, actor_internal(next_state))
        where:
            actor_internal(state) -> action
            critic_internal(state, action) -> Q-value
        Args:
            experiences (Tuple[torch.Tensor]): tuple of (s, a, r, s', done)
        """
        states, actions, rewards, next_states, dones = experience        
        
        # ----------------------- update critic ---------------------------
        # Get predicted next-state actions and Q values from target models
        actions_next = self.actor_internal(next_states)
        Q_targets_next = self.critic_internal(next_states, actions_next)
        
        # Compute Q targets for current states (y_i)
        Q_targets = rewards + (self.gamma * Q_targets_next * (1 - dones))
        
        # Compute critic loss
        Q_expected = self.critic_online(states, actions)
        critic_loss = F.mse_loss(Q_expected, Q_targets)
        
        # Minimize the loss
        self.critic_optimizer.zero_grad()
        if self.clip_critic:
            torch.nn.utils.clip_grad_norm_(self.critic_online.parameters(), 1)
            
        critic_loss.backward()
        self.critic_optimizer.step()

        # ---------------------------- update actor ---------------------------- #
        # Compute actor loss
        actions_pred = self.actor_online(states)
        actor_loss = -self.critic_online(states, actions_pred).mean()
        
        # Minimize the loss
        self.actor_optimizer.zero_grad()
        actor_loss.backward()
        self.actor_optimizer.step()

        # ----------------------- update target networks ----------------------- #
        self.soft_update(self.critic_online, self.critic_internal)
        self.soft_update(self.actor_online, self.actor_internal)                     

    #---------------------------------------------------------------------------
    # Network Update
    #---------------------------------------------------------------------------
    def soft_update(self, online_model, internal_model):
        """Soft update model parameters: θ_internal = τ*θ_online + (1 - τ)*θ_internal
        Args:
            online_model (PyTorch model): Weights will be learned by and copied from
            internal_model (PyTorch model): Internal slow learning the weights are copied to
        """
        for internal_param, online_param in zip(internal_model.parameters(), online_model.parameters()):
            internal_param.data.copy_(self.tau*online_param.data + (1.0-self.tau)*internal_param.data)

    def reset(self):
        self.noise.reset()

        


