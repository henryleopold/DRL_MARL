#!/usr/bin/env python3
""" Visualization Scripts """
__author__ = 'Henry Leopold'

# ==============================================================================
# Imports
# ------------------------------------------------------------------------------
import numpy as np
from IPython import display
from matplotlib import animation
import matplotlib.pyplot as plt
import pandas as pd

# ==============================================================================
# Contents
# ------------------------------------------------------------------------------

__all__ = [
        # ===================
        # Agent related
        # -------------------
            'plot_scores',
            'plot_rolling_scores',
]

# ==============================================================================
# Agent Related
# ------------------------------------------------------------------------------

def plot_scores(scores):
    """Plots the scores from training using matplotlib.
    Args
        scores (list): Numerical list of scores (rewards) during training.
    """
    fig = plt.figure()
    ax = fig.add_subplot(111)
    plt.plot(np.arange(len(scores)), scores)
    plt.ylabel('Score')
    plt.xlabel('Episode #')
    plt.show()

def plot_rolling_scores(scores, rolling_window=100):
    """Plot scores and optional rolling mean using specified window.
    Args
        scores (list): List of episode scores (usually floats)
        rolling_window (int): Window size
    """
    plt.plot(scores); plt.title("Scores");
    rolling_mean = pd.Series(scores).rolling(rolling_window).mean()
    plt.plot(rolling_mean);
    return rolling_mean