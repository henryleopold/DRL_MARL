#!/usr/bin/env python3
""" Environment and training related classes and functions """
__author__ = 'Henry Leopold'

# ==============================================================================
# Imports
# ------------------------------------------------------------------------------
import numpy as np
import random
from collections import deque
from tqdm import trange as tnrange

from unityagents import UnityEnvironment
import utils
import torch

# ==============================================================================
# Contents
# ------------------------------------------------------------------------------

__all__ = [
        # ======================
        # Experiment Utilities
        # ----------------------
            'build_env',
            'load_experiment',
        # ======================
        # Policy optimization
        # ----------------------
            'trainer',
]

# ==============================================================================
# Environment
# ------------------------------------------------------------------------------
def build_env(file_name, no_graphics=False, seed=0):
    env = UnityEnvironment(file_name=file_name, docker_training=True, no_graphics=no_graphics, seed=seed)
    
    # get the default brain
    brain_name = env.brain_names[0]
    brain = env.brains[brain_name]
    
    # reset the environment
    env_info = env.reset(train_mode=True)[brain_name]

    # number of agents
    num_agents = len(env_info.agents)
    print('Number of agents:', num_agents)

    # size of each action
    action_size = brain.vector_action_space_size
    print('Size of each action:', action_size)

    # examine the state space 
    states = env_info.vector_observations
    state_size = states.shape[1]
    print('There are {} agents. Each observes a state with length: {}'.format(states.shape[0], state_size))
    print('The state for the first agent looks like:', states[0])
    return env, brain_name, num_agents, action_size, state_size

    
def load_experiment(filepath, ext='pickle'):
    """ Open and return experiment config and scores """
    scores = utils.load_pickle(filepath+ 'scores.pickle')
    actor = torch.load(filepath +'checkpoint_actor.'+ext)
    critic = torch.load(filepath + 'checkpoint_critic.'+ext)
    try:
        ac = utils.load_pickle(filepath+ 'agent_config.pickle')
        tc = utils.load_pickle(filepath+ 'training_config.pickle')
        ec = utils.load_pickle(filepath+ 'env_config.pickle')
    except:
        print('configs not found')
        return scores, actor, critic
    return scores, actor, critic, ac, tc, ec

# ==============================================================================
# Policy Optimization
# ------------------------------------------------------------------------------
def trainer(learner, env, brain_name, n_episodes=100, max_t=None, print_every=1, checkpoint_freq=250, checkpoint_dir='', reward_window=100, target_reward=30, start_ep=1, add_noise=True, close=True):
    utils.make_dir(checkpoint_dir)
    num_agents = learner.num_agents
    scores_deque = deque(maxlen=reward_window)
    total_scores = [] 
    print(f'\nScores from last episode and the average over {reward_window} will update every {print_every} episodes.\n')               
    with tnrange(start_ep, n_episodes+1) as tn:
        for i_episode in tn:
            env_info = env.reset(train_mode=True)[brain_name]   

            # Get each agents' state
            states = env_info.vector_observations    
            # Initialize score variables and agent    
            learner.reset()
                
            scores = np.zeros(learner.num_agents)
            
            while True:
                actions = learner.act(states, add_noise=add_noise)
                
                env_info = env.step(actions)[brain_name]

                # Get each agents' next state
                next_states = env_info.vector_observations        

                # Get each agents' reward
                rewards = env_info.rewards   

                dones = env_info.local_done 
                
                # Have the agent learn
                learner.step(states, actions, rewards, next_states, dones)
                
                # Update each agents' score
                scores += rewards 
                
                # Update states for next time step
                states = next_states                               

                # Exit / Episode end conditions
                if np.any(dones) or (max_t is not None and step>=max_t): 
                    break
            
            scores_deque.append(np.max(scores))
            total_scores.append(scores)

            if i_episode % print_every == 0 or i_episode==1:
                tn.set_description('Scores: a1: {:.4f} | a2: {:.4f} | (Avg: {:.5f})'.format(scores[0],scores[1],np.mean(scores_deque)))

            if i_episode % checkpoint_freq == 0 or i_episode==n_episodes:
                learner.save(checkpoint_dir+'learner')

            # Early stop condition based on target average score over a predefined episode window
            if np.mean(scores_deque)>=target_reward:
                tn.set_description('\nEnvironment solved in {:d} episodes!\tAverage Score: {:.2f}'.format(i_episode-reward_window, np.mean(scores_deque)))
                learner.save(utils.build_path(checkpoint_dir, 'solved-{:d}'.format(i_episode-reward_window))+'learner')
                break
        if close:
            env.close()
            
        print('Finished training.')
        return total_scores

