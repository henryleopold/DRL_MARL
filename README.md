# Collaboration and Competition - Competitive Tennis Agents

## Challenge Details

This project trains two agents using an actor-critic method on the [Tennis](https://github.com/Unity-Technologies/ml-agents/blob/master/docs/Learning-Environment-Examples.md#tennis) environment using Deep Deterministic Policy Gradients (DDPG) from [Continuous Control with Deep Reinforcement Learning](https://arxiv.org/abs/1509.02971).

![Tennis](assets/tennis.gif)

In this environment, two agents control rackets to bounce a ball over a net. If an agent hits the ball over the net, it receives a reward of +0.1.  If an agent lets a ball hit the ground or hits the ball out of bounds, it receives a reward of -0.01.  Thus, the goal of each agent is to keep the ball in play.

The observation space consists of 8 variables corresponding to the position and velocity of the ball and racket. Each agent receives its own, local observation.  Two continuous actions are available, corresponding to movement toward (or away from) the net, and jumping. 

### Solution Case

The task is episodic, and in order to solve the environment, your agents must get an average score of +0.5 (over 100 consecutive episodes, after taking the maximum over both agents). Specifically,

- After each episode, we add up the rewards that each agent received (without discounting), to get a score for each agent. This yields 2 (potentially different) scores. We then take the maximum of these 2 scores.
- This yields a single **score** for each episode.

The environment is considered solved, when the average (over 100 episodes) of those **scores** is at least +0.5.

## Results
The specific instructions for configuring and running this project are inline within _**Report.ipynb**_, as are the detailed results of the project. The system completed training within 1235 episodes, with an average of 0.50200 beginning on episode 1136. 

# Getting Started
## Quick start
A docker image was created for running DRLND experiments, which can be found [here](https://hub.docker.com/r/henryleopold/drlnd/). It requires NVIDIA-Cuda and [nvidia-docker](https://github.com/NVIDIA/nvidia-docker) and the correct Unity environment be downloaded and extracted into the *assets* folder. The filepath in the .ipynb needs to be updated to reflect your filepath. Before starting the notebook, install required python packages by running:

    pip install .

To start, first modify the local project directory to match your project folder (mine is '~/Projects/drlnd') and then run:
```
docker run --runtime=nvidia -it -p 8888:8888 -v ~/Projects/drlnd:/root/drlnd  henryleopold/drlnd:unity
```

## Package Installation 
Please refer to the instructions for setting up the [DRLND source](https://www.udacity.com/course/deep-reinforcement-learning-nanodegree--nd893) and [Unity ML-Agents](https://github.com/Unity-Technologies/ml-agents/blob/master/docs/Installation.md) repositories.

Instructions for installing nvidia-docker are [here](https://github.com/NVIDIA/nvidia-docker).

# Instructions
Once the docker is running, connect to the jupyter notebook and train using the ipython notebook. The build, agent and training config variables, which are python dictionaries defininng parameters. Tweak the dictionaries within the notebooks to modify hyperparameters.



